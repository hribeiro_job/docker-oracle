#! / bin / bash
# LICENSE UPL 1.0
#
# Copyright (c) 1982-2018 Oracle e / ou suas afiliadas. Todos os direitos reservados.
#
# Desde: maio de 2017
# Autor: gerald.venzl@oracle.com
# Descrição: verifica o status do banco de dados Oracle.
# Return codes: 0 = PDB está aberto e pronto para uso
#               1 = PDB não está aberto
#               2 = Falha na execução do Sql Plus
# NÃO ALTERE OU REMOVA AVISOS DE DIREITOS AUTORAIS OU ESTE CABEÇALHO.
#
ORACLE_SID="`grep $ORACLE_HOME /etc/oratab | cut -d: -f1`"
OPEN_MODE="READ WRITE"
ORAENV_ASK=NO
source oraenv

[ -f "$ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/oratab" ] || exit 1;

# Verifique no Oracle pelo menos um PDB com open_mode "READ WRITE" e armazene-o no status
status=`su -p oracle -c "sqlplus -s / as sysdba" << EOF
   set heading off;
   set pagesize 0;
   SELECT DISTINCT open_mode FROM v\\$pdbs WHERE open_mode = '$OPEN_MODE';
   exit;
EOF`

# Store return code from SQL*Plus
ret=$?

# SQL Plus execution was successful and PDB is open
if [ $ret -eq 0 ] && [ "$status" = "$OPEN_MODE" ]; then
   exit 0;
# PDB is not open
elif [ "$status" != "$OPEN_MODE" ]; then
   exit 1;
# SQL Plus execution failed
else
   exit 2;
fi;
