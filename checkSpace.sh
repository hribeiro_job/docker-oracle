#! / bin / bash
# LICENSE UPL 1.0
#
# Copyright (c) 1982-2018 Oracle e / ou suas afiliadas. Todos os direitos reservados.
#
# Desde: janeiro de 2017
# Autor: gerald.venzl@oracle.com
# Descrição: verifica o espaço disponível do sistema.
#
# NÃO ALTERE OU REMOVA AVISOS DE DIREITOS AUTORAIS OU ESTE CABEÇALHO.
#

REQUIRED_SPACE_GB=13
AVAILABLE_SPACE_GB=`df -PB 1G / | tail -n 1 | awk '{ print $4 }'`

if [ $AVAILABLE_SPACE_GB -lt $REQUIRED_SPACE_GB ]; then
  script_name=`basename "$0"`
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "$script_name: ERROR - There is not enough space available in the docker container."
  echo "$script_name: The container needs at least $REQUIRED_SPACE_GB GB, but only $AVAILABLE_SPACE_GB GB are available."
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  exit 1;
fi;
