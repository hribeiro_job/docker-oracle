#! / bin / bash
# LICENSE UPL 1.0
#
# Copyright (c) 1982-2018 Oracle e / ou suas afiliadas. Todos os direitos reservados.
#
# Desde: novembro de 2016
# Autor: gerald.venzl@oracle.com
# Descrição: define a senha para sys, system e pdb_admin
#
# NÃO ALTERE OU REMOVA AVISOS DE DIREITOS AUTORAIS OU ESTE CABEÇALHO.
#

ORACLE_PWD=$1
ORACLE_SID="`grep $ORACLE_HOME /etc/oratab | cut -d: -f1`"
ORACLE_PDB="`ls -dl $ORACLE_BASE/oradata/$ORACLE_SID/*/ | grep -v pdbseed | awk '{print $9}' | cut -d/ -f6`"
ORAENV_ASK=NO
source oraenv

su -p oracle -c "sqlplus / as sysdba << EOF
      ALTER USER SYS IDENTIFIED BY "$ORACLE_PWD";
      ALTER USER SYSTEM IDENTIFIED BY "$ORACLE_PWD";
      ALTER SESSION SET CONTAINER=$ORACLE_PDB;
      ALTER USER PDBADMIN IDENTIFIED BY "$ORACLE_PWD";
      exit;
EOF"
